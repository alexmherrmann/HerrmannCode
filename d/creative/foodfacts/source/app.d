import std.stdio;
import std.conv : to;
import std.file : exists, readText;
import std.format : format;
import std.string : indexOf;
import std.csv : csvReader;
import std.range.primitives;
import std.traits;


//struct Maybe!float {
//  float value = -1;
//  bool hasValue = false;
//  this(string inputValue) {
//    if(inputValue.length > 0) {
//      hasValue = true;
//      value = inputValue.to!float;
//    }
//  }
//}



import fooditem : FoodItem, Maybe;

const string filename = "data/FoodFacts.csv";
// we'll start by just using the gzipped csv file
import std.algorithm.iteration : each, filter, map;
import std.array : array;
import std.parallelism : TaskPool, task, Task;

public alias FilterPredicate = bool delegate(FoodItem fi);

void filterFunction(FoodItem[]* storein, FoodItem[] _records, FilterPredicate predicate) {
  auto filtered =  filter!predicate(_records).array;
  stderr.writefln("filtered size: %d", filtered.length);
  *storein = filtered;
}

void doCsv() {
  bool fileExists;
  fileExists = exists(filename);
  if(!fileExists) {
    stderr.writeln("couldn't find %s".format(filename));
    return;
  }

  string decompressed = readText(filename);
  decompressed = decompressed[decompressed.indexOf("\n")+1..$-1];
  auto records = csvReader!FoodItem(decompressed).array;

  FilterPredicate predicate = (FoodItem item) {
    return (item.salt_100g.hasValue && item.fat_100g.hasValue) && (item.salt_100g.value < 100 && item.fat_100g.value < 100) ;
  };

  FilterPredicate nrgPredicate = (FoodItem item) {
    return (item.energy_100g.hasValue && item.salt_100g.hasValue);
  };

  FoodItem[] fieldsWithSaltValue;
  FoodItem[] fieldsWithSugarAndEnergy;

  TaskPool filters = new TaskPool(3);

  filters.put(task!filterFunction(&fieldsWithSaltValue, records, predicate));
  filters.put(task!filterFunction(&fieldsWithSugarAndEnergy, records, nrgPredicate));

  filters.finish(true);

  stderr.writeln("finished filtering tasks...");

  auto report = (string name, FoodItem[] a) { stderr.writefln("%s has %d elements", name, a.length); };
  report("salt", fieldsWithSaltValue);
  report("nrg", fieldsWithSugarAndEnergy);

  TaskPool plots = new TaskPool(5);

  import saltfat : basicSaltFatPlot, basicSaltFatHistogram;
  import energy : sugarVsEnergy;

  plots.put(task!basicSaltFatPlot(fieldsWithSaltValue));
  plots.put(task!basicSaltFatHistogram(fieldsWithSaltValue));
  plots.put(task!sugarVsEnergy(fieldsWithSugarAndEnergy));

  plots.finish(true);

  stderr.flush();
  stdout.flush();
}


void main()  {
//  writeln("foodfacts stuff!");
  doCsv();
}
