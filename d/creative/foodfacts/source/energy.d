﻿module energy;

import ggplotd.aes,
  ggplotd.ggplotd,
  ggplotd.geom,
  ggplotd.legend,
  ggplotd.axes;

import std.range.primitives : isInputRange;
import std.algorithm.iteration : map;
import std.stdio;
import std.array : array;

import fooditem : FoodItem, Maybe;

//auto sugNrg(FoodItem item) {
//  return ;
//}

void sugarVsEnergy(T)(T range) if (isInputRange!T) {
  stderr.writeln("sugar plot!");
  GGPlotD plot =  GGPlotD();
  plot.put(cast(XAxisFunction) (XAxis xaxis) {
      xaxis.label = "sugar content";
      xaxis.min = 0;
      xaxis.max = 100;
      return xaxis;
    });
  plot.put(cast(YAxisFunction) (YAxis yaxis) {
      yaxis.label = "energy content";
      yaxis.min = 0;
      yaxis.max = 100;
      return yaxis;
    });

  range.map!(item => aes!("x", "y", "size")(item.sugars_100g.value, item.energy_100g.value, .15)).sugNrg.geomPoint.array.putIn(plot);


  plot.save("sugarEnergy.svg");


}
