﻿module saltfat;

import std.algorithm.iteration : each, filter, map;
import std.range : zip;
import std.stdio;
import std.conv : to;
import std.format : format;
import std.range.primitives;
import std.traits;
import fooditem : FoodItem;
import std.array : array;

import ggplotd.aes,
  ggplotd.ggplotd,
  ggplotd.geom,
  ggplotd.legend,
  ggplotd.axes;

auto report = (string name, float[] a) {stderr.writefln("%s has %d items, first 5 are %s", name, a.length, a[0..5]);};

void basicSaltFatHistogram(T)(T range) if (isInputRange!T) {
  stderr.writeln("basic histogram started");
  GGPlotD plot = GGPlotD();

  auto fat = range.map!((FoodItem i) {return i.fat_100g.value;}).array;
  auto salt = range.map!((FoodItem i) {return i.salt_100g.value;}).array;


  auto hey = () {
    XAxisFunction xsetup = (XAxis axis) {
      axis.label = "Fat content per 100g";
      axis.min = 0;
      axis.max = 100;
      axis.show = true;
      return axis;
    };

    YAxisFunction ysetup = (YAxis axis) {
      axis.label = "Sugar content per 100g";
      axis.min = 0;
      axis.max = 100;
      axis.show = true;
      return axis;
    };

    plot.put(xsetup);
    plot.put(ysetup);
  };
  hey();

  auto hist = zip(fat, salt).map!(t => aes!("x", "y")(t[0], t[1])).geomHist2D(200,200);
  plot.put(hist);
  plot.put(discreteLegend);

  plot.save("basicHist.svg");
  stderr.writeln("basic histogram finished");
}

void basicSaltFatPlot(T)(T range) if (isInputRange!T) {
  stderr.writeln("basic plot started");
  GGPlotD plot = range.map!((FoodItem item) =>
    aes!("x","y", "size")(item.fat_100g.value, item.salt_100g.value, 0.20)).array.geomPoint.putIn(GGPlotD());

  XAxisFunction xsetup = (XAxis axis) {
    axis.label = "Fat content per 100g";
    axis.min = 0;
    axis.max = 100;
    axis.show = true;
    return axis;
  };

  YAxisFunction ysetup = (YAxis axis) {
    axis.label = "Sugar content per 100g";
    axis.min = 0;
    axis.max = 100;
    axis.show = true;
    return axis;
  };

  plot.put(xsetup);
  plot.put(ysetup);

  plot.save("basicPlot.svg");
  stderr.writeln("basic plot finished");
}
