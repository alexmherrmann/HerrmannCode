import std.stdio;
import std.array;

int main(string[] args) {
	auto toprint = join(args[1..$], ' ');
	writeln(toprint);
	return 0;
}
