import core.thread;
import std.stdio;

class DerivedFiber : Fiber {
		this() {
				super( &run );
		}

private :
		void run() {
				printf( "Derived fiber running.\n" );
		}
}

void fiberFunc(int n) {
		printf( "Composed fiber running. %d\n", n);
		Fiber.yield();
		printf( "Composed fiber running. %d\n", n);
}


void main(string[] args) {
	// create instances of each type
	Fiber derived = new DerivedFiber();
	int n2 = 6;
	Fiber composed = new Fiber( () {fiberFunc(n2);} );

	// call both fibers once
	derived.call();
	composed.call();
	printf( "Execution returned to calling context.\n" );
	composed.call();

	// since each fiber has run to completion, each should have state TERM
	assert( derived.state == Fiber.State.TERM );
	assert( composed.state == Fiber.State.TERM );
}
