package main

import "fmt"

func main() {
	var test1 = new(string)
	fmt.Println(test1)
	fmt.Println(*test1) // runtime error

	*test1 = "Hello World"
	fmt.Println(test1)
	fmt.Println(*test1)
}
